<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Bien::class, inversedBy="reservations")
     */
    private $bien;

    /**
     * @ORM\OneToMany(targetEntity=Utilisateur::class, mappedBy="reservation")
     */
    private $locataire;



    /**
     * @ORM\Column(type="integer")
     */
    private $nb_adultes;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nb_enfants;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_deb;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_fin;

    /**
     * @ORM\OneToMany(targetEntity=Utilisateur::class, mappedBy="reservation")
     */
    private $utilisateurs;

    public function __construct()
    {
        $this->locataire = new ArrayCollection();
        $this->utilisateurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBien(): ?Bien
    {
        return $this->bien;
    }

    public function setBien(?Bien $bien): self
    {
        $this->bien = $bien;

        return $this;
    }

    /**
     * @return Collection|utilisateur[]
     */
    public function getLocataire(): Collection
    {
        return $this->locataire;
    }

    public function addLocataire(utilisateur $locataire): self
    {
        if (!$this->locataire->contains($locataire)) {
            $this->locataire[] = $locataire;
            $locataire->setReservation($this);
        }

        return $this;
    }

    public function removeLocataire(utilisateur $locataire): self
    {
        if ($this->locataire->contains($locataire)) {
            $this->locataire->removeElement($locataire);
            // set the owning side to null (unless already changed)
            if ($locataire->getReservation() === $this) {
                $locataire->setReservation(null);
            }
        }

        return $this;
    }


    public function getNbAdultes(): ?int
    {
        return $this->nb_adultes;
    }

    public function setNbAdultes(int $nb_adultes): self
    {
        $this->nb_adultes = $nb_adultes;

        return $this;
    }

    public function getNbEnfants(): ?string
    {
        return $this->nb_enfants;
    }

    public function setNbEnfants(int $nb_enfants): self
    {
        $this->nb_enfants = $nb_enfants;

        return $this;
    }

    public function getDateDeb(): ?\DateTimeInterface
    {
        return $this->date_deb;
    }

    public function setDateDeb(\DateTimeInterface $date_deb): self
    {
        $this->date_deb = $date_deb;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->date_fin;
    }

    public function setDateFin(\DateTimeInterface $date_fin): self
    {
        $this->date_fin = $date_fin;

        return $this;
    }

    /**
     * @return Collection|Utilisateur[]
     */
    public function getUtilisateurs(): Collection
    {
        return $this->utilisateurs;
    }

    public function addUtilisateur(Utilisateur $utilisateur): self
    {
        if (!$this->utilisateurs->contains($utilisateur)) {
            $this->utilisateurs[] = $utilisateur;
            $utilisateur->setReservation($this);
        }

        return $this;
    }

    public function removeUtilisateur(Utilisateur $utilisateur): self
    {
        if ($this->utilisateurs->contains($utilisateur)) {
            $this->utilisateurs->removeElement($utilisateur);
            // set the owning side to null (unless already changed)
            if ($utilisateur->getReservation() === $this) {
                $utilisateur->setReservation(null);
            }
        }

        return $this;
    }
}
