<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\Utilisateur;
use App\Entity\Bien;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $faker = Faker\Factory::create("fr_FR");

        for($i = 0; $i < 10; $i++){ // boucle de création des users
            $user = new Utilisateur();
            $user->setNom($faker->lastName)
                ->setPrenom($faker->firstName)
                ->setAdresse($faker->address)
                ->setEmail($faker->email)
                ->setTelephone($faker->phoneNumber)
                ->setPassword($this->encoder->encodePassword($user, "000"))
                ->setRole($faker->company);
            $manager->persist($user);
            $this->addReference("user-" . $i, $user);
        }

        for($i = 0; $i < 90; $i++)
        { // boucle de création des biens
            $bien = new Bien();
            if ( $i < 30 ) {
                $type = "Emplacement";
                $places = rand(8, 12);
            } elseif ($i < 80) {
                $type = "Mobile-home";
                $places = rand(3, 8);
            } else {
                $type = "Caravane";
                $places = rand(2, 6);
            }

            $user = new Utilisateur();

            $bien->setTitre($faker->word)
                ->setUtilisateur($this->getReference('user-' . rand(0, 9)))
                ->setType($type)
                ->setDescCourte($faker->text)
                ->setDescLongue($faker->text)
                ->setPlaces($places);


            if ( $type == "Mobile-home" ) {
                switch ($places) {
                    case 3:
                        $bien->setPrix(20);
                        break;
                    case 4:
                        $bien->setPrix(24);
                        break;
                    case 5:
                        $bien->setPrix(27);
                        break;
                    case $places > 5 and $places <= 8:
                        $bien->setPrix(34);
                        break;
                    default:
                        $bien->setPrix(20);
                }
            } elseif( $type == "Caravane" ) {
                switch ($places) {
                    case 2: //Dc 3
                        $bien->setPrix(15);
                        break;
                    case $places > 2 and $places <= 4:
                        $bien->setPrix(18);
                        break;
                    case $places > 4 and $places <= 6:
                        $bien->setPrix(24);
                        break;
                    default:
                        $bien->setPrix(15);
                }
            } else {
                switch ($places) {
                    case 8:
                        $bien->setPrix(12);
                        break;
                    case $places > 8:
                        $bien->setPrix(14);
                        break;
                    default:
                        $bien->setPrix(14);
                }

            }

            $manager->persist($bien);
            $this->addReference("bien-" . $i, $bien);


        }


        $manager->flush();
    }
}
