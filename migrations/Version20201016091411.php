<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201016091411 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bien (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, utilisateur_id INTEGER DEFAULT NULL, type VARCHAR(255) NOT NULL, desc_courte VARCHAR(255) NOT NULL, desc_longue VARCHAR(255) NOT NULL, prix INTEGER NOT NULL, places VARCHAR(255) DEFAULT NULL, titre VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_45EDC386FB88E14F ON bien (utilisateur_id)');
        $this->addSql('CREATE TABLE facture (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, reservation_id INTEGER DEFAULT NULL, prix INTEGER NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FE866410B83297E7 ON facture (reservation_id)');
        $this->addSql('CREATE TABLE reservation (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, bien_id INTEGER DEFAULT NULL, nb_adultes INTEGER NOT NULL, nb_enfants VARCHAR(255) NOT NULL, date_deb DATETIME NOT NULL, date_fin DATETIME NOT NULL)');
        $this->addSql('CREATE INDEX IDX_42C84955BD95B80F ON reservation (bien_id)');
        $this->addSql('CREATE TABLE utilisateur (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, reservation_id INTEGER DEFAULT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, telephone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, role VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_1D1C63B3B83297E7 ON utilisateur (reservation_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE bien');
        $this->addSql('DROP TABLE facture');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('DROP TABLE utilisateur');
    }
}
