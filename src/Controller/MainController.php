<?php


namespace App\Controller;

use App\Entity\Bien;
use App\Entity\Reservation;
use App\Entity\Utilisateur;
use App\Form\FormReservationType;
use App\Repository\BienRepository;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    private $repository;

    /**
     * @Route("/", name="accueil", methods={"GET", "POST"})
     * @param BienRepository $repo
     * @return Response
     */
    public function homePage(BienRepository $repo)
    {
        $biens = $repo->findBy( [], null, 3);

        return $this->render('front/accueil.html.twig', [
            "biens" => $biens,
            "image" => "emplacement"
        ]);
    }

    /**
     * @Route("/emplacements", name="emplacement", methods={"GET"})
     * @param BienRepository $repo
     * @return Response
     */
    public function emplacement(BienRepository $repo)
    {
        $emplacements = $repo->findBy(['type' => 'Emplacement']);


        return $this->render("front/emplacement.html.twig", [
            "type" => $emplacements,
            "image" => "emplacement"
        ]);
    }

    /**
     * @Route("/caravanes", name="caravane", methods={"GET"})
     * @param BienRepository $repo
     * @return Response
     */
    public function caravane(BienRepository $repo)
    {
        $caravanes = $repo->findBy(['type' => 'Caravane']);

        return $this->render("front/caravane.html.twig", [
            "type" => $caravanes,
            "image" => "caravane"
        ]);
    }

    /**
     * @Route("/mobile-homes", name="mobile-home", methods={"GET"})
     * @param BienRepository $repo
     * @return Response
     */
    public function mobile_home(BienRepository $repo)
    {
        $mobile_home = $repo->findBy(['type' => 'Mobile-home']);

        return $this->render("front/mobile-home.html.twig", [
            "type" => $mobile_home,
            "image" => "mobile-home"
        ]);
    }

    /**
     * @Route("/detail{id}", name="detail", methods={"GET", "POST"})
     * @param BienRepository $repo
     * @param int $id
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function detail(BienRepository $repo, int $id, Request $request, EntityManagerInterface $entityManager)
    {
        $biens = $repo->findBy(['id' =>  $id]);
        $proprietaire = $this->getDoctrine()->getRepository(Utilisateur::class)->findBy(['id' => $biens[0]->getUtilisateur()]);


        $reservation = new Reservation();

        $form = $this->createForm(FormReservationType::class, $reservation);
        $form->handleRequest($request);


        if ( $form->isSubmitted()) {
            $data = $form->getData();
            $reservation->setBien($biens[0]);
            $reservation->addUtilisateur($proprietaire[0]);

            $entityManager->persist($data);
            $entityManager->persist($reservation);

            $entityManager->flush();
        }

        return $this->render("front/detail.html.twig", [
            "biens" => $biens,
            "id" => $id,
            'form' => $form->createView()
        ]);
    }











}