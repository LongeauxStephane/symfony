<?php

namespace App\Form;

use App\Entity\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nb_adultes', null, [
                "label" => "Nombres adultes"
            ])
            ->add('nb_enfants', null, [
                "label" => "Nombres enfants"
            ])
            ->add('date_deb', null, [
                "label" => "Date de d'arrivée"
            ])
            ->add('date_fin', null, [
                "label" => "Date de sortie"
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
