<?php


namespace App\Controller;

use App\Entity\Bien;
use App\Repository\BienRepository;
use App\Repository\ReservationRepository;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/backoffice")
 */
class BackendController extends AbstractController
{

    /**
     * @Route("/", name="BackHomePage")
     */
    public function backOffice()
    {
        return $this->render("back/backoffice.html.twig", []);
    }

    /**
     * @Route("/locations", name="locations", methods={"GET"})
     */
    public function locations(ReservationRepository $repo)
    {
        $locations = $repo->findAll();

        return $this->render("back/personnel/locations.html.twig", [
            'locations' => $locations
        ]);
    }

    /**
     * @Route("/Facturations", name="Facturations", methods={"GET"})
     */
    public function Facturations()
    {
        return $this->render("back/personnel/Facturations.html.twig", []);
    }

    /**
     * @Route("/Exploitation", name="Exploitation", methods={"GET"})
     */
    public function Exploitation()
    {
        return $this->render("back/personnel/Exploitation.html.twig", []);
    }

}